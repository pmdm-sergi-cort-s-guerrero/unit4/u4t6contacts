package dam.android.sergic.u4t6contacts;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import dam.android.sergic.u4t6contacts.model.ContactItem;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>
{
    /* attributes. */
    private MyContacts myContacts;

    // TODO Act2: Two new listener attributes.
    private OnItemClickListener listenerOnClick;
    private OnItemLongClickListener listenerOnLongClick;

    //------------------------------------------------------------------------------
    // TODO Act2: Using a listener pattern.
    // Interface that specify the listener behavior.
    public interface OnItemClickListener
    {
        void onItemClickListener(ContactItem info);
    }

    public interface OnItemLongClickListener
    {
        boolean onItemLongClickListener(ContactItem info);
    }

    // ------------------------------------------------------------------------------
    // Class for each item: contains only a TextView.
    static class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvId;
        TextView tvName;
        TextView tvNumber;
        ImageView ivThumbnail;

        public MyViewHolder(View view)
        {
            super(view);
            tvId = view.findViewById(R.id.tvID);
            tvName = view.findViewById(R.id.tvName);
            tvNumber = view.findViewById(R.id.tvNumber);
            ivThumbnail = view.findViewById(R.id.ivThumbnail);
        }

        // sets viewHolder views with data
        public void bind(final ContactItem contactData, final OnItemClickListener onClick,
                         final OnItemLongClickListener onLongClick)
        {
            tvId.setText(contactData.getContactId()+"");
            tvName.setText(contactData.getDisplayName());
            tvNumber.setText(contactData.getNumber());

            Uri image = contactData.getThumbnail();
            if(image!=null)
                ivThumbnail.setImageURI(image);
            else
                ivThumbnail.setImageResource(R.drawable.no_thumbnail);

            // TODO Act2: Setting up the listeners.
            this.itemView.setOnClickListener(l -> onClick.onItemClickListener(contactData));
            this.itemView.setOnLongClickListener(x -> onLongClick.onItemLongClickListener(contactData));
        }
    }
    // ------------------------------------------------------------------------------

    // constructor: myContacts contains contacts data.
    MyAdapter(MyContacts myContacts, OnItemClickListener onClick, OnItemLongClickListener onLongClick)
    {
        this.myContacts = myContacts;
        this.listenerOnClick = onClick;
        this.listenerOnLongClick = onLongClick;
    }

    // TODO Act2 2: Loading the contacts again.
    public void refreshContacts()
    {
        myContacts.refreshData();
        notifyDataSetChanged();
    }

    // Creates a new view item: Layout Manager calls this method.
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        // Create item view:
        // use a simple textView predefined layout that contains only TextView.
        View contactView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_contact_view,parent,false);

        return new MyViewHolder(contactView);
    }

    // Replaces the data content of viewHolder (recycles old viewHolder): Layout Manager calls this method
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
    {
        // bind viewHolder with data at: position
        holder.bind(myContacts.getContactData(position), listenerOnClick, listenerOnLongClick);
    }

    // returns the size of dataSet: Layout Manager calls this method.
    @Override
    public int getItemCount()
    {
        return myContacts.getCount();
    }
}