package dam.android.sergic.u4t6contacts;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Objects;
import dam.android.sergic.u4t6contacts.model.ContactItem;
import static androidx.recyclerview.widget.RecyclerView.*;

// TODO Act2 1: Implementing the right interfaces.
public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener,
        MyAdapter.OnItemLongClickListener
{
    MyContacts myContacts;
    RecyclerView recyclerView;
    private TextView tvShowInfo;

    // constant
    private final static int VIEW_CONTACT_REQUEST = 0;

    // Permissions required to contacts provider, only needed to READ
    private static String[] PERMISSIONS_CONTACTS = {Manifest.permission.READ_CONTACTS};

    // Id to identify a contacts permission request
    private static final int REQUEST_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

        if(checkPermissions())
            setListAdapter();
    }

    private void setUI()
    {
        recyclerView = findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);

        // set recycleView with a linear layout manager.
        recyclerView.setLayoutManager(new LinearLayoutManager(this
                , VERTICAL,false));

        // TODO Act2 1.3: By default the textView isn't visible.
        tvShowInfo = findViewById(R.id.tvShowInfo);
        tvShowInfo.setVisibility(View.INVISIBLE);
    }

    private void setListAdapter()
    {
        // MyContacts class gets data from ContactProvider
        myContacts = new MyContacts(this);

        // TODO Act2 1: Setting to my adapter the listeners.
        // set adapter to recycleView
        recyclerView.setAdapter(new MyAdapter(myContacts, this,this));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(),
                DividerItemDecoration.VERTICAL));

        // TODO Act2 1.3: When there's a movement in the list, the textView is hided.
        recyclerView.addOnScrollListener(new OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);

                if(tvShowInfo.getVisibility()==View.VISIBLE)
                    tvShowInfo.setVisibility(View.INVISIBLE);
            }
        });

        // Hide empty list TextView
        if(myContacts.getCount() > 0)
            findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);
    }

    private boolean checkPermissions()
    {
        // check for permissions granted before setting listView Adapter data
        if(ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED)
        {
            // opens Dialog: requests user to grant permission.
            ActivityCompat.requestPermissions(this,MainActivity.PERMISSIONS_CONTACTS, MainActivity.REQUEST_CONTACTS);
            return false;
        }
        else
            return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions
            , @NonNull int[] grantResults)
    {
        if(requestCode == REQUEST_CONTACTS)
        {
            // We have requested one READ permission for contacts, so only need [0] to be checked.
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                setListAdapter();
            else
                Toast.makeText(this,getString(R.string.contacts_read_right_required),Toast.LENGTH_LONG).show();
        }
        else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onItemClickListener(ContactItem info)
    {
        // TODO Act2 1.4: Making the textView visible when onClick happens.
        tvShowInfo.setText(info.toString());
        tvShowInfo.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onItemLongClickListener(ContactItem info)
    {
        // TODO Act2 2: When a long click is detected launch the contact data
        //  to be visualized or edited.

        tvShowInfo.setVisibility(View.INVISIBLE);               // Hiding the textView with the contact info.

        // Creating an special intent with the action to accomplish and the URI to know what contact
        // show.
        Uri uri = ContactsContract.Contacts.getLookupUri(info.getContactId(), info.getLookupKey());
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);

        // Start the activity for result.
        startActivityForResult(intent,VIEW_CONTACT_REQUEST);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        // TODO Act2 2: If there was any change into a contact, call adapter for refreshing the list.
        if(requestCode == VIEW_CONTACT_REQUEST && resultCode>=0)
            ((MyAdapter) Objects.requireNonNull(recyclerView.getAdapter())).refreshContacts();
    }
}