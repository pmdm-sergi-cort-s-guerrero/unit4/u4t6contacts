package dam.android.sergic.u4t6contacts.model;

import android.net.Uri;
import android.provider.ContactsContract;
import androidx.annotation.NonNull;

public class ContactItem
{
    private long _id;
    private long contactId;
    private long rawContactId;
    private String lookupKey;
    private String displayName;
    private String number;
    private int phoneType;
    private Uri thumbnail;

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public long getContactId() {
        return contactId;
    }

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    public long getRawContactId() {
        return rawContactId;
    }

    public void setRawContactId(long rawContactId) {
        this.rawContactId = rawContactId;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(int phoneType) {
        this.phoneType = phoneType;
    }

    public Uri getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Uri thumbnail) {
        this.thumbnail = thumbnail;
    }

    // TODO Act2 1.1: Showing all contact information.
    @NonNull
    @Override
    public String toString()
    {
        String type;

        // TODO Act2 1.2: 'Translating' type number to a string value.
        switch(phoneType)
        {
            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                type = "HOME";
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                type = "WORK";
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                type = "MOBILE";
                break;
            default:
                type = "OTHER";
                break;
        }

        return displayName +" "+number+" ("+type+")\n"+
                "_ID: "+_id+" CONTACT_ID: "+contactId +" RAW_CONTACT_ID: "+rawContactId+"\n"+
                "LOOKUP_KEY: " + lookupKey;
    }
}