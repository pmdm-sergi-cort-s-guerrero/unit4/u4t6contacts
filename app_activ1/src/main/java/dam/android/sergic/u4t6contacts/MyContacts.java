package dam.android.sergic.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import dam.android.sergic.u4t6contacts.model.ContactItem;

public class MyContacts
{
    private ArrayList<ContactItem> myDataSet;
    private Context context;

    public MyContacts(Context context)
    {
        this.context = context;
        this.myDataSet = getContacts();
    }

    // Get contacts list from ContactsProvider
    private ArrayList<ContactItem> getContacts()
    {
        ArrayList<ContactItem> contactsList = new ArrayList<>();

        // access to ContentProviders
        ContentResolver contentResolver = context.getContentResolver();

        // TODO Act1 1.1: Selecting more information.
        // aux variables
        String[] projection = new String[]{
                                        ContactsContract.Data._ID,
                                        ContactsContract.Data.CONTACT_ID,
                                        ContactsContract.Data.LOOKUP_KEY,
                                        ContactsContract.Data.RAW_CONTACT_ID,
                                        ContactsContract.Data.DISPLAY_NAME,
                                        ContactsContract.CommonDataKinds.Phone.NUMBER,
                                        ContactsContract.CommonDataKinds.Phone.TYPE,                // Type of phone (WORK, MOBILE...)
                                        ContactsContract.Data.PHOTO_THUMBNAIL_URI
//                                        ContactsContract.Data.PHOTO_URI
                                        };

        String selectionFilter = ContactsContract.Data.MIMETYPE + "='"+
                                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE +"' AND "+
                                ContactsContract.CommonDataKinds.Phone.NUMBER+" IS NOT NULL";

        // query required data
        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,            // URI to Contacts: content://com.android.contacts/data
                projection,                                                                         // projection
                selectionFilter,                                                                    // selection filter
                null,                                                                 // sortOrder
                ContactsContract.Data.DISPLAY_NAME+" ASC");

        if(contactsCursor != null)
        {
            // TODO Act1 1.2: Getting the new information.
            // get the column indexes for desired name and number columns.
            int idIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.CONTACT_ID);
            int rawIdIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
            int lookupIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.LOOKUP_KEY);
            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int typeIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE);
//            int photoId = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.PHOTO_URI);                                  // High res
            int photoId = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.PHOTO_THUMBNAIL_URI);                      // Low res.

            ContactItem contact;
            // read data and add to ArrayList
            while(contactsCursor.moveToNext())
            {
                contact = new ContactItem();

                contact.setContactId(contactsCursor.getLong(idIndex));
                contact.setRawContactId(contactsCursor.getLong(rawIdIndex));
                contact.setLookupKey(contactsCursor.getString(lookupIndex));
                contact.setDisplayName(contactsCursor.getString(nameIndex));
                contact.setNumber(contactsCursor.getString(numberIndex));
                contact.setPhoneType(contactsCursor.getInt(typeIndex));

                String uri = contactsCursor.getString(photoId);
                if(uri!=null)
                    contact.setThumbnail(Uri.parse(uri));

                contactsList.add(contact);
            }
            contactsCursor.close();
        }

        return contactsList;
    }

    public ContactItem getContactData(int position)
    {
        return myDataSet.get(position);
    }
    public int getCount()
    {
        return myDataSet.size();
    }
}