package dam.android.sergic.u4t6contacts.model;

import android.net.Uri;

// TODO Act3: Adding all the needed code, this class included.
public class ContactItem
{
    private long contactId;
    private long rawContactId;
    private String lookupKey;
    private String displayName;
    private String number;
    private int phoneType;
    private Uri thumbnail;

    public ContactItem()
    {
        this.thumbnail = null;
    }

    public long getContactId() {
        return contactId;
    }

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    public long getRawContactId() {
        return rawContactId;
    }

    public void setRawContactId(long rawContactId) {
        this.rawContactId = rawContactId;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(int phoneType) {
        this.phoneType = phoneType;
    }

    public Uri getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Uri thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "ContactItem{" +
                "contactId='" + contactId + '\'' +
                ", rawContactId=" + rawContactId +
                ", lookupKey='" + lookupKey + '\'' +
                ", displayName='" + displayName + '\'' +
                ", number='" + number + '\'' +
                ", phoneType=" + phoneType +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
}