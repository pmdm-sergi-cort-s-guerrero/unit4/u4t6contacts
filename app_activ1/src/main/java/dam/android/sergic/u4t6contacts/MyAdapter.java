package dam.android.sergic.u4t6contacts;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import dam.android.sergic.u4t6contacts.model.ContactItem;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>
{
    private MyContacts myContacts;

    // ------------------------------------------------------------------------------
    // Class for each item: contains only a TextView.
    static class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvId;
        TextView tvName;
        TextView tvNumber;
        ImageView ivThumbnail;

        public MyViewHolder(View view)
        {
            // TODO Act2 2.2: Getting all elements.
            super(view);
            tvId = view.findViewById(R.id.tvID);
            tvName = view.findViewById(R.id.tvName);
            tvNumber = view.findViewById(R.id.tvNumber);
            ivThumbnail = view.findViewById(R.id.ivThumbnail);
        }

        // sets viewHolder views with data
        public void bind(ContactItem contactData)
        {
            // TODO Act2 2.3: Binding the contact data.
            tvId.setText(contactData.getContactId()+"");
            tvName.setText(contactData.getDisplayName());
            tvNumber.setText(contactData.getNumber());

            // TODO Act2 2.4: Showing a default image if there isn't anyone.
            Uri image = contactData.getThumbnail();
            if(image!=null)
                ivThumbnail.setImageURI(image);
            else
                ivThumbnail.setImageResource(R.drawable.no_thumbnail);
        }
    }
    // ------------------------------------------------------------------------------

    // constructor: myContacts contains contacts data.
    MyAdapter(MyContacts myContacts)
    {
        this.myContacts = myContacts;
    }

    // Creates a new view item: Layout Manager calls this method.
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        // Create item view:
        // TODO Act2 2.1: Loading the new constraint layout.
        View contactView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_contact_view,parent,false);

        return new MyViewHolder(contactView);
    }

    // Replaces the data content of viewHolder (recycles old viewHolder): Layout Manager calls this method
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
    {
        // bind viewHolder with data at: position
        holder.bind(myContacts.getContactData(position));
    }

    // returns the size of dataSet: Layout Manager calls this method.
    @Override
    public int getItemCount()
    {
        return myContacts.getCount();
    }
}